<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

include 'pdoConnect.php';


   // Retrieve the posted data
   $json    =  file_get_contents('php://input');
   $obj     =  json_decode($json);
  

  // Sanitise URL supplied values
         $firstName 		     = filter_var($obj->firstName, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $emailAddress	  = filter_var($obj->emailAddress, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $lastName 		     = filter_var($obj->name, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $uuid	  = filter_var($obj->uuid, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $phoneNumber   = filter_var($obj->phoneNumber, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);

         // Attempt to run PDO prepared statement
         try {
            $sql 	= "INSERT INTO users( uuid,firstName, emailAddress, lastName, phoneNumber) VALUES(:uuid, :firstName, :lastName, :emailAddress,  phoneNumber,)";
            $stmt 	= $pdo->prepare($sql);
			$stmt->bindParam(':uuid', $uuid, PDO::PARAM_STR);
            $stmt->bindParam(':firstName', $firstName, PDO::PARAM_STR);
            $stmt->bindParam(':lastName', $lastName, PDO::PARAM_STR);
			$stmt->bindParam(':emailAddress', $emailAddress, PDO::PARAM_STR);
		    $stmt->bindParam(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
            $stmt->execute();

            echo json_encode(array('message' => 'Congratulations the record ' . $firstName . ' was added to the database'));
         }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
         }
		 
		 ?>