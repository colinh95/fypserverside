<?php


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

   // Define database connection parameters
   $hn      = 'localhost';
   $un      = 'root';
   $pwd     = 'tester';
   $db      = 'fyp';
   $cs      = 'utf8';

   // Set up the PDO parameters
   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo 	= new PDO($dsn, $un, $pwd, $opt);


   // Retrieve the posted data
   $json    =  file_get_contents('php://input');
   $obj     =  json_decode($json);
   

  

         // Sanitise URL supplied values
         $userId 		     = filter_var($obj->userId, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $nodeOwnerId	  = filter_var($obj->nodeOwnerId, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $address	  = filter_var($obj->address, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $chargerType	  = filter_var($obj->chargerType, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $startTime	  = filter_var($obj->startTime, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $finishTime	  = filter_var($obj->finishTime, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $nodeId		 = filter_var($obj->nodeId, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $nodeAddress		 = filter_var($obj->nodeAddress, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $cost		 = filter_var($obj->cost, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
				
	     		 	
		
				
					
					
		
	
         // Attempt to run PDO prepared statement
         try {
            $sql 	= "INSERT INTO bookings(
			userId, 
			nodeOwnerId, 
			cost,
			nodeAddress, 
			chargerType, 
			startTime, 
			finishTime,
			nodeId,
			userFirstName,
			userLastName
			) 
			VALUES
			(
			'$userId', 
			'$nodeOwnerId',
			'$cost',
			'$nodeAddress', 
			'$chargerType', 
			'$startTime', 
			'$finishTime',
			'$nodeId',
			(SELECT firstName FROM  user where uuid = '$userId'),
			(SELECT lastName FROM  user where uuid = '$userId')
			)";
            $stmt 	= $pdo->prepare($sql);
			$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
			$stmt->bindParam(':nodeOwnerId', $nodeOwnerId, PDO::PARAM_STR);
			$stmt->bindParam(':cost', $cost, PDO::PARAM_STR);
			$stmt->bindParam(':nodeId', $nodeId, PDO::PARAM_STR);
			$stmt->bindParam(':nodeAddress', $nodeAddress, PDO::PARAM_STR);
			$stmt->bindParam(':chargerType', $chargerType, PDO::PARAM_STR);
			$stmt->bindParam(':startTime', $startTime, PDO::PARAM_STR);
			$stmt->bindParam(':finishTime', $finishTime, PDO::PARAM_STR);
			
            $stmt->execute();
			
			
			//$user = mysqli_query($con, "SELECT firstName FROM  user where uuid = '7ETPTj7glATR10UVEcXR1TcAb8d2'");   
		
			
			
			
			
			
			
			
			
			
			
			
			
			

            echo json_encode(array('message' => 'Congratulations the record ' . $name . ' was added to the database'));
         }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
			//	fwrite($myfile,$e);
			//fclose($myfile);
         }
		 /*
		 
		 $myfile = fopen("output.txt", "w") or die("Unable to open file!");
				
				fwrite($myfile,"ADDRESS: ");
				fwrite($myfile,$nodeAddress);
				fwrite($myfile,"  + OWNERID: ");
				fwrite($myfile,$nodeOwnerId);
				fwrite($myfile,"  + NODEID:");
				fwrite($myfile,$nodeId);
					fwrite($myfile,"  + USERID:");
				fwrite($myfile,$userId);
				fwrite($myfile,"  + ");
				fwrite($myfile,$chargerType);
				fwrite($myfile,"   + ");
				fwrite($myfile,$startTime);
				fwrite($myfile,"   + ");
				fwrite($myfile,$finishTime);
			fclose($myfile);
			
			fwrite($myfile,"   ");
*/
    

?>


