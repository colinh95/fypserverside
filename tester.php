<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

   // Define database connection parameters
   $hn      = 'localhost';
   $un      = 'root';
   $pwd     = 'tester';
   $db      = 'fyp';
   $cs      = 'utf8';

   // Set up the PDO parameters
   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo 	= new PDO($dsn, $un, $pwd, $opt);


   // Retrieve the posted data
   $json    =  file_get_contents('php://input');
   $obj     =  json_decode($json);





     
	     		 	
	
         // Attempt to run PDO prepared statement
       try {
			 
			$uuid 		     = 'GavKm50SnTdc9EprhX1yp1NQNHk1';
			$lat		     = '2';
			$lng	  = '12313';
			$address	  = '234g3qeqfc';
			$chargerType	  = 'ev';
			$startTime	  = '03:00';
			$finishTime	  = '04:00';
			
			  //  Sanitise URL supplied values
         $uuid1 		     = filter_var($uuid, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $lat1		     = filter_var($lat, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $lng1	  = filter_var($lng, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $address1	  = filter_var($address, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $chargerType1	  = filter_var($chargerType, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $startTime1	  = filter_var($startTime, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $finishTime1	  = filter_var($finishTime, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 			
			
            $sql 	= "INSERT INTO markers(
				address,
				lat,
				lng,
				chargerType,
				startTime,
				finishTime,
				uuid
			) 
			SELECT 
				'$address1', 
				'$lat1', 
				'$lng1', 
				'$chargerType1', 
				'$startTime1', 
				'$finishTime1',
				uuid 
			FROM user 
			WHERE uuid = '$uuid'
			LIMIT 1 ";
		
			$stmt 	= $pdo->prepare($sql);
			$stmt->bindParam(':uuid', $uuid, PDO::PARAM_STR);
            $stmt->bindParam(':lat', $lat, PDO::PARAM_STR);
            $stmt->bindParam(':lng', $lng, PDO::PARAM_STR);
		    $stmt->bindParam(':address', $address, PDO::PARAM_STR);
			$stmt->bindParam(':chargerType', $chargerType, PDO::PARAM_STR);
			$stmt->bindParam(':startTime', $startTime, PDO::PARAM_STR);
			$stmt->bindParam(':finishTime', $finishTime, PDO::PARAM_STR);
	
            $stmt->execute();
   }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
         }
?>