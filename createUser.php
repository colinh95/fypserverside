<?php


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

   // Define database connection parameters
   $hn      = 'localhost';
   $un      = 'root';
   $pwd     = 'tester';
   $db      = 'fyp';
   $cs      = 'utf8';

   // Set up the PDO parameters
   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo 	= new PDO($dsn, $un, $pwd, $opt);


   // Retrieve the posted data
   $json    =  file_get_contents('php://input');
   $obj     =  json_decode($json);
   $key     =  strip_tags($obj->key);


   // Determine which mode is being requested
   switch($key)
   {

      // Add a new record to the users table
      case "create":

         // Sanitise URL supplied values
         $uuid 		     = filter_var($obj->uuid, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $firstName 		     = filter_var($obj->firstName, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $lastName	  = filter_var($obj->lastName, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		 $emailAddress	  = filter_var($obj->emailAddress, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
	     $phoneNumber   = filter_var($obj->phoneNumber, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
		
	
         // Attempt to run PDO prepared statement
         try {
            $sql 	= "INSERT INTO users(uuid, firstName, lastName, emailAddress, phoneNumber) VALUES(:uuid, :firstName, :lastName, :emailAddress, :phoneNumber)";
            $stmt 	= $pdo->prepare($sql);
			$stmt->bindParam(':uuid', $uuid, PDO::PARAM_STR);
            $stmt->bindParam(':firstName', $firstName, PDO::PARAM_STR);
            $stmt->bindParam(':lastName', $lastName, PDO::PARAM_STR);
		    $stmt->bindParam(':emailAddress', $emailAddress, PDO::PARAM_STR);
		    $stmt->bindParam(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
            $stmt->execute();

            echo json_encode(array('message' => 'Congratulations the record ' . $name . ' was added to the database'));
         }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
         }

      break;



    
      case "update":
	  
      break;
   }

?>