<?php


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

   // Define database connection parameters
   $hn      = 'localhost';
   $un      = 'root';
   $pwd     = 'tester';
   $db      = 'fyp';
   $cs      = 'utf8';

   // Set up the PDO parameters
   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo 	= new PDO($dsn, $un, $pwd, $opt);


   // Retrieve the posted data
   $json    =  file_get_contents('php://input');
   $obj     =  json_decode($json);
  



      // Add a new record to the markers table
   

          //Sanitise URL supplied values
         $uuid 		     = filter_var($obj->uuid, FILTER_SANITIZE_STRING);
		 $lat		     = filter_var($obj->lat, FILTER_SANITIZE_STRING);
         $lng	  = filter_var($obj->lng, FILTER_SANITIZE_STRING);
		 $address	  = filter_var($obj->address, FILTER_SANITIZE_STRING);
		 $chargerType	  = filter_var($obj->chargerType, FILTER_SANITIZE_STRING);
		 $startTime	  = filter_var($obj->startTime, FILTER_SANITIZE_STRING);
		 $finishTime	  = filter_var($obj->finishTime, FILTER_SANITIZE_STRING);
		 $costPer15Mins 		     = filter_var($obj->costPer15Mins, FILTER_SANITIZE_STRING);
		 
		 
	     		 	
	
             // Attempt to run PDO prepared statement
       try { 
		
            $sql 	= "INSERT INTO markers(
				nodeAddress,
				lat,
				lng,
				chargerType,
				startTime,
				finishTime,
				costPer15Mins,
				nodeOwnerId
			
			) 
			SELECT 
				'$address', 
				'$lat', 
				'$lng', 
				'$chargerType', 
				'$startTime', 
				'$finishTime',
				'$costPer15Mins',
				uuid 
			FROM user 
			WHERE uuid = '$uuid'
			LIMIT 1 ";
		
			$stmt 	= $pdo->prepare($sql);
			$stmt->bindParam(':uuid', $uuid, PDO::PARAM_STR);
            $stmt->bindParam(':lat', $lat, PDO::PARAM_STR);
            $stmt->bindParam(':lng', $lng, PDO::PARAM_STR);
		    $stmt->bindParam(':address', $address, PDO::PARAM_STR);
			$stmt->bindParam(':chargerType', $chargerType, PDO::PARAM_STR);
			$stmt->bindParam(':startTime', $startTime, PDO::PARAM_STR);
			$stmt->bindParam(':finishTime', $finishTime, PDO::PARAM_STR);
			$stmt->bindParam(':costPer15Mins', $costPer15Mins, PDO::PARAM_STR);
	
            $stmt->execute();
			
			
			$sql 	= "INSERT INTO markers(
				nodeAddress,
				lat,
				lng,
				chargerType,
				startTime,
				finishTime,
				costPer15Mins,
				nodeOwnerId
			
			) 
			SELECT 
				'$address', 
				'$lat', 
				'$lng', 
				'$chargerType', 
				'$startTime', 
				'$finishTime',
				'$costPer15Mins',
				uuid 
			FROM user 
			WHERE uuid = '$uuid'
			LIMIT 1 ";
		
			$stmt 	= $pdo->prepare($sql);
			
			
			
			
			
			

               }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
		
			
         }


?>